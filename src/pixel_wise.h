#pragma once

#include <array>

#include <GL/gl.h>

namespace PW {
	enum Anchor
	{
		MID_LEFT,
		TOP_LEFT,
		TOP_MID,
		TOP_RIGHT,
		MID_RIGHT,
		BOTTOM_RIGHT,
		BOTTOM_MID,
		BOTTOM_LEFT,
		CENTER,
		TOP_NONE,
		MID_NONE,
		BOTTOM_NONE,
		NONE_LEFT,
		NONE_MID,
		NONE_RIGHT,
	};

	/// Convert pixel-space x and y to relative-space (-1 -> 0 -> 1) coords
	std::array<GLfloat, 2> toRelative(GLfloat x, GLfloat y, Anchor anchor);
	/// Mid-Left `toRelative`
	inline std::array<GLfloat, 2> ml(GLfloat x, GLfloat y) {
		return PW::toRelative(x, y, PW::MID_LEFT);
	}
	/// Top-Left `toRelative`
	inline std::array<GLfloat, 2> tl(GLfloat x, GLfloat y) {
		return PW::toRelative(x, y, PW::TOP_LEFT);
	}
	/// Top-Mid `toRelative`
	inline std::array<GLfloat, 2> tm(GLfloat x, GLfloat y) {
		return PW::toRelative(x, y, PW::TOP_MID);
	}
	/// Top-Right `toRelative`
	inline std::array<GLfloat, 2> tr(GLfloat x, GLfloat y) {
		return PW::toRelative(x, y, PW::TOP_RIGHT);
	}
	/// Mid-Right `toRelative`
	inline std::array<GLfloat, 2> mr(GLfloat x, GLfloat y) {
		return PW::toRelative(x, y, PW::MID_RIGHT);
	}
	/// Bottom-Right `toRelative`
	inline std::array<GLfloat, 2> br(GLfloat x, GLfloat y) {
		return PW::toRelative(x, y, PW::BOTTOM_RIGHT);
	}
	/// Bottom-Mid `toRelative`
	inline std::array<GLfloat, 2> bm(GLfloat x, GLfloat y) {
		return PW::toRelative(x, y, PW::BOTTOM_MID);
	}
	/// Bottom-Left `toRelative`
	inline std::array<GLfloat, 2> bl(GLfloat x, GLfloat y) {
		return PW::toRelative(x, y, PW::BOTTOM_LEFT);
	}
	/// Center `toRelative`
	inline std::array<GLfloat, 2> c(GLfloat x, GLfloat y) {
		return PW::toRelative(x, y, PW::CENTER);
	}
	/// Top-None `toRelative`
	inline std::array<GLfloat, 2> tn(GLfloat x, GLfloat y) {
		return PW::toRelative(x, y, PW::TOP_NONE);
	}
	/// Mid-None `toRelative`
	inline std::array<GLfloat, 2> mn(GLfloat x, GLfloat y) {
		return PW::toRelative(x, y, PW::MID_NONE);
	}
	/// Bottom-None `toRelative`
	inline std::array<GLfloat, 2> bn(GLfloat x, GLfloat y) {
		return PW::toRelative(x, y, PW::BOTTOM_NONE);
	}
	/// None-Left `toRelative`
	inline std::array<GLfloat, 2> nl(GLfloat x, GLfloat y) {
		return PW::toRelative(x, y, PW::NONE_LEFT);
	}
	/// None-Mid `toRelative`
	inline std::array<GLfloat, 2> nm(GLfloat x, GLfloat y) {
		return PW::toRelative(x, y, PW::NONE_MID);
	}
	/// None-Right `toRelative`
	inline std::array<GLfloat, 2> nr(GLfloat x, GLfloat y) {
		return PW::toRelative(x, y, PW::NONE_RIGHT);
	}
}
