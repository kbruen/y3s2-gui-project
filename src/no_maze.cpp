#include "no_maze.h"

#include <string>

#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glut.h>

#include "pixel_wise.h"

void NoMazeScreen::display() {
	glClear(GL_COLOR_BUFFER_BIT);

	glColor3ub(0xFF, 0x00, 0x00);
	glRasterPos2fv(PW::tm(-100, 20).data());
	for (const auto& c : std::string{"No Maze Loaded"}) {
		glutBitmapCharacter(GLUT_BITMAP_HELVETICA_18, c);
	}

	glFlush();
	glutSwapBuffers();
}

void NoMazeScreen::keyboard(unsigned char key, int x, int y) {
	if (key == '\e') {
		exit(EXIT_SUCCESS);
	}
}
