#include "pixel_wise.h"

#include <GL/glut.h>
#include "utils.hpp"

// For personal reference:
// x - left->right
// y - bottom->top

std::array<GLfloat, 2> PW::toRelative(GLfloat x, GLfloat y, PW::Anchor anchor) {
	auto winWidth = glutGet(GLUT_WINDOW_WIDTH);
	auto winHeight = glutGet(GLUT_WINDOW_HEIGHT);

	std::array<GLfloat, 2> result;

	switch (anchor) {
		case PW::BOTTOM_LEFT:
		case PW::MID_LEFT:
		case PW::TOP_LEFT:
		case PW::NONE_LEFT:
			result[0] = Utils::nummap(x, (GLfloat)0, (GLfloat)winWidth, (GLfloat)-1, (GLfloat)1);
			break;
		case PW::BOTTOM_MID:
		case PW::CENTER:
		case PW::TOP_MID:
		case PW::NONE_MID:
			result[0] = Utils::nummap(x, -((GLfloat)winWidth) / 2, ((GLfloat)winWidth) / 2, (GLfloat)-1, (GLfloat)1);
			break;
		case PW::BOTTOM_RIGHT:
		case PW::MID_RIGHT:
		case PW::TOP_RIGHT:
		case PW::NONE_RIGHT:
			result[0] = Utils::nummap(x, -(GLfloat)winWidth, (GLfloat)0, (GLfloat)-1, (GLfloat)1);
			break;
		default:
			result[0] = x;
		}

	switch (anchor) {
		case PW::TOP_LEFT:
		case PW::TOP_MID:
		case PW::TOP_RIGHT:
		case PW::TOP_NONE:
			result[1] = Utils::nummap(y, (GLfloat)0, (GLfloat)winHeight, (GLfloat)1, (GLfloat)-1);
			break;
		case PW::MID_LEFT:
		case PW::CENTER:
		case PW::MID_RIGHT:
		case PW::MID_NONE:
			result[1] = Utils::nummap(y, -((GLfloat)winHeight) / 2, ((GLfloat)winHeight) / 2, (GLfloat)1, (GLfloat)-1);
			break;
		case PW::BOTTOM_LEFT:
		case PW::BOTTOM_MID:
		case PW::BOTTOM_RIGHT:
		case PW::BOTTOM_NONE:
			result[1] = Utils::nummap(y, (GLfloat)0, -(GLfloat)winHeight, (GLfloat)1, (GLfloat)-1);
			break;
		default:
			result[1] = y;
		}

	return std::move(result);
}
