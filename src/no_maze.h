#pragma once

#include "glut_events.h"

struct NoMazeScreen : public GlutEvents {
	virtual void display();
	virtual void keyboard(unsigned char key, int x, int y);
};
