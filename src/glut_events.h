#pragma once

struct GlutEvents {
	virtual void display() {}
	virtual void overlayDisplay() {}
	virtual void reshape(int width, int height) {}
	virtual void keyboard(unsigned char key, int x, int y) {}
	virtual void keyboardUp(unsigned char key, int x, int y) {}
	virtual void mouse(int button, int state, int x, int y) {}
	virtual void motion(int x, int y) {}
	virtual void passiveMotion(int x, int y) {}
	virtual void visibility(int state) {}
	virtual void entry(int state) {}
	virtual void special(int key, int x, int y) {}
	virtual void idle() {}
};
