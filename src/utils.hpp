#pragma once

namespace Utils {
	const double PI = 3.14159265358979323846;

	template <typename T>
	T nummap(T input, T inMin, T inMax, T outMin, T outMax) {
		// From: https://www.arduino.cc/reference/en/language/functions/math/map/
		return (input - inMin) * (outMax - outMin) / (inMax - inMin) + outMin;
	}

	template <typename T>
	T clamp(T input, T min, T max) {
		if (input < min) {
			return min;
		}
		else if (input > max) {
			return max;
		}
		else {
			return input;
		}
	}

	template <typename T>
	constexpr T toRad(T deg) {
		return deg * PI / 180;
	}

	template <typename T>
	constexpr T min(T a, T b) {
		if (a < b) {
			return a;
		}
		else {
			return b;
		}
	}
}
