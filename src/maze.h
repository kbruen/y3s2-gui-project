#pragma once

#include "glut_events.h"

#include <vector>

class MazeScreen : public GlutEvents {
private:
	std::vector<std::vector<int>> maze;
	int angleX;
	bool mouseCapture;
	float posX;
	float posZ;
	bool solid;
	float forceX;
	float forceZ;

public:
	MazeScreen(const std::vector<std::vector<int>> &maze);

	virtual void display();
	virtual void keyboard(unsigned char key, int x, int y);
	virtual void keyboardUp(unsigned char key, int x, int y);
	virtual void idle();
	virtual void passiveMotion(int x, int y);
	virtual void mouse(int button, int state, int x, int y);
};
