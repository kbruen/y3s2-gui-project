#include <fstream>
#include <vector>
#include <memory>
#include <string>

#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glut.h>

#include "glut_events.h"
#include "utils.hpp"
#include "no_maze.h"
#include "maze.h"

std::vector<std::vector<int>> maze;
std::unique_ptr<GlutEvents> events;

void display(void) {
	if (events) {
		events->display();
	}
}
void overlayDisplay(void) {
	if (events) {
		events->overlayDisplay();
	}
}
void reshape(int width, int height) {
	if (events) {
		events->reshape(width, height);
	}
}
void keyDown(unsigned char key, int x, int y) {
	if (events) {
		events->keyboard(key, x, y);
	}
}
void keyUp(unsigned char key, int x, int y) {
	if (events) {
		events->keyboardUp(key, x, y);
	}
}
void mouse(int button, int state, int x, int y) {
	if (events) {
		events->mouse(button, state, x, y);
	}
}
void motion(int x, int y) {
	if (events) {
		events->motion(x, y);
	}
}
void passiveMotion(int x, int y) {
	if (events) {
		events->passiveMotion(x, y);
	}
}
void visibility(int state) {
	if (events) {
		events->visibility(state);
	}
}
void entry(int state) {
	if (events) {
		events->entry(state);
	}
}
void special(int key, int x, int y) {
	if (events) {
		events->special(key, x, y);
	}
}
void idle() {
	if (events) {
		events->idle();
	}
}

void load_maze(const std::string& path) {
	std::ifstream f{path};
	std::string line;

	while(true) {
		std::getline(f, line);
		if (!f) {
			break;
		}

		std::vector<int> row;
		for (const auto& c: line) {
			switch(c) {
			case ' ':
				row.push_back(0);
				break;
			case 'X':
				row.push_back(1);
				break;
			case 'G':
				row.push_back(2);
				break;
			default:
				row.push_back(-1);
			}
		}
		if (row.size() > 0) {
			maze.push_back(row);
		}
	}
}

int main(int argc, char** argv)
{
	std::string maze_file;
	if (argc < 2) {
		events = std::make_unique<NoMazeScreen>();
	}
	else {
		maze_file = std::string{argv[1]};
		load_maze(maze_file);
		events = std::make_unique<MazeScreen>(maze);
	}

	glutInit(&argc, argv);

	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA | GLUT_DEPTH);
	glClearDepth(1.0f);
	glDepthFunc(GL_LEQUAL);


	glutInitWindowSize(640, 480);
	glutInitWindowPosition((glutGet(GLUT_SCREEN_WIDTH) - 640) * 0.5, (glutGet(GLUT_SCREEN_HEIGHT) - 480) * 0.5);
	if (maze_file.size() > 0) {
		glutCreateWindow((std::string{"Maze - "} + maze_file).c_str());
	}
	else {
		glutCreateWindow("Maze");
	}

	if (maze_file.size() > 0) {
		glEnable(GL_DEPTH_TEST);
		glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
		glMatrixMode(GL_PROJECTION);
		glLoadIdentity();
		gluPerspective(60, 1, 0.1, 100);
	}

	glutDisplayFunc(display);
	glutOverlayDisplayFunc(overlayDisplay);
	glutReshapeFunc(reshape);
	glutSetKeyRepeat(GLUT_KEY_REPEAT_OFF);
	glutKeyboardFunc(keyDown);
	glutKeyboardUpFunc(keyUp);
	glutMouseFunc(mouse);
	glutMotionFunc(motion);
	glutPassiveMotionFunc(passiveMotion);
	glutVisibilityFunc(visibility);
	glutEntryFunc(entry);
	glutSpecialFunc(special);
	glutIdleFunc(idle);
	glutMainLoop();
	return 0;
}
