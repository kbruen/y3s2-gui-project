LD_PARAMS := -lglut -lGL -lGLU
SRC := src
OBJ := obj
CXX := g++
LD := g++

DESTDIR?=/
PREFIX=/usr
INSTALL?=install

SOURCES := $(wildcard $(SRC)/*.cpp)
OBJECTS := $(patsubst $(SRC)/%.cpp, $(OBJ)/%.o, $(SOURCES))

.PHONY: all
all: build

.PHONY: build
build: program

.PHONY: install
install:
	${INSTALL} -Dm755 program ${DESTDIR}${PREFIX}/bin/y3s2-gui-project

.PHONY: clean
clean:
	rm -f program
	rm -rf obj

program: ${OBJECTS}
	$(LD) ${LD_PARAMS} $^ -o $@

$(OBJECTS): $(OBJ)/%.o: $(SRC)/%.cpp
	$(CXX) ${CXX_PARAMS} $< -c -o $@
