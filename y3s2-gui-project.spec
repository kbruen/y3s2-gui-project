Name:           y3s2-gui-project
Version:        1.0.7
Release:        1%{?dist}
Summary:        Maze project in OpenGL for Graphics and User Interfaces lecture in year 3, semester 2

License:        UNLICENSED
URL:            https://codeberg.org/kbruen/%{name}
Source0:        https://codeberg.org/kbruen/%{name}/archive/v%{version}.tar.gz

BuildRequires:  mesa-libGLU-devel
BuildRequires:  freeglut-devel
BuildRequires:  desktop-file-utils

%description
Maze project in OpenGL for Graphics and User Interfaces lecture in year 3, semester 2

%global debug_package %{nil}

%prep
%autosetup -n %{name}


%build
mkdir obj
%make_build


%install
%make_install
desktop-file-install --dir=%{buildroot}%{_datadir}/applications %{name}.desktop


%files
%{_bindir}/%{name}
%{_datadir}/applications/%{name}.desktop

